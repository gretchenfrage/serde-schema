This serialization system is designed around the idea that that a _schema_,
a specification for what values are permitted and how they're encoded as
raw bytes, is a data structure that can be manipulated programmatically
at runtime and itself serialized. This can be used to achieve
bincode-levels of efficiency, protobufs levels of validation, and JSON
levels of easy debugging. For example, one could arrange a key/value store
such that the store contains, on-disk, the serialized schemas for the keys
and the values. Or, an RPC protocol could be designed such that, upon
initialization, the server sends down its list of endpoints and the
serialized schemas for their parameters and return types.

Typical usage pattern:

- create `CoderStateAlloc`
- to encode (serialize) a value:
    1. combine `&Schema` and `CoderStateAlloc` into `CoderState`
    2. combine `&mut CoderState` and `&mut W` where `W: Write` into `Encoder`
    3. pass `&mut Encoder` and `&`value into procedure for encoding value
    4. on `CoderState`, call `.is_finished_or_err()?` to guarantee that
       valid schema-comformant data was fully written to `W`
    5. convert `CoderState` back into `CoderStateAlloc` so it can be reused
- to decode (deserialize) a value:
    1. combine `&Schema` and `CoderStateAlloc` into `CoderState`
    2. combine `&mut CoderState` and `&mut R` where `R: Read` into `Decoder`
    3. pass `&mut Decoder` into procedure for decoding value
    4. on `CoderState`, call `.is_finished_or_err()?` to guarantee that
       valid schema-comformant data was fully read from `R`, and no more
    5. convert `CoderState` back into `CoderStateAlloc` so it can be reused

The data model supports:

- `u8` through `u128`, `i8` through `i128`(32 bits and above are encoded
   variable length)
- `f32` and `f64`, `char`, `bool`
- utf8 string, byte string
- option
- fixed length array, variable length array
- tuple (just values back-to-back)
- struct (just values back-to-back, but at schema-time the fields have 
  names)
- enum, as in rust-style enum, as in tagged union, as in "one of"
- recursing up in the schema, so as to support recursive schema types like
  trees

The tests illustrate how serializing even a recursive data tree in a schema-
checked way is easy and natural:

```rust
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, KnownSchema)]
pub enum BinaryTree {
    Branch {
        value: u32,
        left: Box<BinaryTree>,
        right: Box<BinaryTree>,
    },
    Leaf(u32),
}

#[test]
fn binary_tree_test() {
    round_trip_test(BinaryTree::Branch {
        value: 5,
        left: Box::new(BinaryTree::Leaf(2)),
        right: Box::new(BinaryTree::Branch {
            value: 10,
            left: Box::new(BinaryTree::Leaf(7)),
            right: Box::new(BinaryTree::Leaf(20)),
        }),
    });
}

fn round_trip_test<T>(val: T)
where
    T: Debug + PartialEq + Serialize + for<'d> Deserialize<'d> + KnownSchema,
{

    // prep
    let schema = T::schema(Default::default());
    let mut coder_alloc = CoderStateAlloc::new();
    let mut buf = Vec::new();

    println!("{:#?}", val);
    println!("{}", schema.pretty_fmt());

    // serialize
    let mut coder = CoderState::new(&schema, coder_alloc, None);
    let mut encoder = Encoder::new(&mut coder, &mut buf);
    val.serialize(&mut encoder)
        .map_err(|e| println!("{}", e))
        .unwrap();
    coder.is_finished_or_err().unwrap();
    coder_alloc = coder.into_alloc();

    println!("{:?}", buf);

    // deserialize
    let mut coder = CoderState::new(&schema, coder_alloc, None);
    let mut read = buf.as_slice();
    let mut decoder = Decoder::new(&mut coder, &mut read);
    let val2 = T::deserialize(&mut decoder)
        .map_err(|e| println!("{}", e))
        .unwrap();
    coder.is_finished_or_err().unwrap();
    coder_alloc = coder.into_alloc();

    println!("{:#?}", val2);
    assert_eq!(val, val2);

    drop(coder_alloc);
}
```

That test prints this recursive schema:

```
- enum <------------------------\-\
  variant 0 (name = "Branch"):  | |
    - struct                    | |
      field 0 (name = "value"): | |
        - u32                   | |
      field 1 (name = "left"):  | |
        - recurse (level = 2) --/ |
      field 2 (name = "right"):   |
        - recurse (level = 2) ----/
  variant 1 (name = "Leaf"):
    - u32
```

And this list of encoded bytes:

```
[0, 5, 1, 2, 0, 10, 1, 7, 1, 20]
```
